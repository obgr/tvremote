import { RawChannel } from '../interfaces/rawChannel';

export class Channel {
  number: number;
  id: string;
  name: string;
  group: string;

  constructor(rawChannel: RawChannel) {
    this.number = rawChannel.number;
    this.id = rawChannel.id;
    this.name = rawChannel.name.split(",")[0];
    this.group = rawChannel.group;
  }
}
