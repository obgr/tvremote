import { RawEpg } from '../interfaces/rawEpg';

export class Epg {
    public startTime: string;
    public endTime: string;
    public title: string;
    public subtitle: string;
    public description: string;
    public id: number;
    public channel: {id: string, name?: string, number?: number, group?: string};
    public vpsStartTime: string;
    public startDay: string;
    public endDay: string;

    constructor(rawEpg: RawEpg) {
        const startTime = new Date(rawEpg.start * 1000);
        const endTime = new Date(rawEpg.start * 1000 + rawEpg.duration * 1000);
        const vpsStartTime = new Date(rawEpg.vpsStart * 1000);
        this.startDay = undefined;
        const days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

        this.id = rawEpg.id;
        this.channel = {id: rawEpg.channel.id};
        this.title = rawEpg.title;
        this.subtitle = rawEpg.shortText;
        this.description = rawEpg.description;
        this.startTime = `${this.pad(startTime.getHours())}:${this.pad(startTime.getMinutes())}`;
        this.endTime = `${this.pad(endTime.getHours())}:${this.pad(endTime.getMinutes())}`;
        this.vpsStartTime = `${this.pad(vpsStartTime.getHours())}:${this.pad(vpsStartTime.getMinutes())}`;
        if (startTime.getDate() !== endTime.getDate()) {
            this.endDay = `${days[endTime.getDay()]}, ${endTime.toLocaleDateString()}`;
        }
        this.startDay = `${days[startTime.getDay()]}, ${startTime.toLocaleDateString()}`;
    }

    private pad(num: number) : string {
        let strNum = num.toString();
        while (strNum.length < 2) {
            strNum = "0" + strNum;
        }
        return strNum;
    }
}
