import { RawTimer } from '../interfaces/rawTimer';

export class Timer {
    number: number;
    isActive: boolean;
    isVps: boolean;
    channel: number;
    date: string;
    startTime: string;
    endTime: string;
    priority: number;
    lifeTime: number;
    name: string;
    description: string;
    startDay: string;
    endDay: string;
  
    constructor(rawTimer: RawTimer) {

        this.number = rawTimer.number;
        this.isActive = rawTimer.isActive;
        this.isVps = rawTimer.isVps;
        this.channel = rawTimer.channel;
        this.date = rawTimer.date;
        if (rawTimer.start.length === 3) {
            this.startTime = `${rawTimer.start.substr(0, 1)}:${rawTimer.start.substr(1, 2)}`;
        } else {
            this.startTime = `${rawTimer.start.substr(0, 2)}:${rawTimer.start.substr(2, 2)}`;
        }
        if (rawTimer.end.length === 3) {
            this.endTime = `${rawTimer.end.substr(0, 1)}:${rawTimer.end.substr(1, 2)}`;
        } else {
            this.endTime = `${rawTimer.end.substr(0, 2)}:${rawTimer.end.substr(2, 2)}`;
        }
        this.priority = rawTimer.priority;
        this.lifeTime = rawTimer.lifeTime;
        this.name = rawTimer.name;
        this.description = rawTimer.description;
    }
  }
  