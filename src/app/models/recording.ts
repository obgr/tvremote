import { RawRecording } from '../interfaces/rawRecording';
import { TreeNode } from '../interfaces/treeNode';

export class Recording implements TreeNode {
  number: number;
  name: string;
  startDate: string;
  startTime: string;
  isNew: boolean;
  level?: number;
  expandable?: boolean;
  children?: Recording[];

  constructor(rawRecording: RawRecording) {
    this.number = rawRecording.number;
    this.name = rawRecording.name;
    this.startDate = rawRecording.startDate;
    this.startTime = rawRecording.startTime;
    this.isNew = rawRecording.isNew;
  }
}
