import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { JsonrpcRequest } from '../interfaces/jsonrpcRequest';
import { JsonrpcResponse } from '../interfaces/jsonrpcResponse';

@Injectable({
  providedIn: 'root'
})
export class JsonrpcBaseService {
  protected jsonRpcUrl = 'http://localhost:8888';  // URL to web api

  constructor(protected http: HttpClient) {
    // Do nothing
  }

  protected executeJsonRpcRequest(method: string, params?: any): Observable<JsonrpcResponse> {
    const jsonRpcBody = this.createJsonrpcRequestString(method, params);
    return this.http.post<JsonrpcResponse>(this.jsonRpcUrl, jsonRpcBody).pipe(
      // eslint-disable-next-line no-console
      tap(_ => console.log(`${method} completed`)),
      catchError(this.handleError<JsonrpcResponse>(method, null))
    );
  }

  private createJsonrpcRequestString(method: string, params?: any): string {
    const jsonrpcRequest: JsonrpcRequest = {
      "jsonrpc": "2.0",
      "id": Math.round(Math.random() * 9998) + 1,
      "method": method,
      "auth": "76dbf15b-b883-40be-a3ca-8c1c8ae1213d"
    };
    if (params !== undefined) {
      jsonrpcRequest.params = params;
    }

    return JSON.stringify(jsonrpcRequest);
  }

   
  // eslint-disable-next-line default-param-last
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // eslint-disable-next-line no-console
      console.error(error); // log to console instead
  
      // eslint-disable-next-line no-console
      console.log(`${operation} failed: ${error.message}`);
  
      return of(result as T);
    };
  }
}
