import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { JsonrpcBaseService } from './jsonrpc-base.service';
import { JsonrpcResponse } from '../interfaces/jsonrpcResponse';

@Injectable({
  providedIn: 'root'
})
export class ChannelService extends JsonrpcBaseService {

  getChannels(): Observable<JsonrpcResponse> {
    return this.executeJsonRpcRequest("getChannels", {max: 40}); // FIXME: Get this value from user settings
  }
}
