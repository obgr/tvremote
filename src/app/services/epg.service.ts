import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { JsonrpcBaseService } from './jsonrpc-base.service';
import { JsonrpcResponse } from '../interfaces/jsonrpcResponse';

@Injectable({
  providedIn: 'root'
})
export class EpgService extends JsonrpcBaseService {

  getEpgs(channel?: number, time?: string): Observable<JsonrpcResponse> {
    let params: any = undefined;
    if (channel !== undefined) {
      params = {channel: channel};
    } else if (time !== undefined) {
      params = {time: time};
    }
    return this.executeJsonRpcRequest("getEpgData", params);
  }
}
