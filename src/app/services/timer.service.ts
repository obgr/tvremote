import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { JsonrpcBaseService } from './jsonrpc-base.service';
import { JsonrpcResponse } from '../interfaces/jsonrpcResponse';

@Injectable({
  providedIn: 'root'
})
export class TimerService extends JsonrpcBaseService {

  getTimers(): Observable<JsonrpcResponse> {
    return this.executeJsonRpcRequest("getTimers");
  }
}
