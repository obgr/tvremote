import { TestBed } from '@angular/core/testing';

import { JsonrpcBaseService } from './jsonrpc-base.service';

describe('JsonrpcBaseService', () => {
  let service: JsonrpcBaseService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(JsonrpcBaseService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
