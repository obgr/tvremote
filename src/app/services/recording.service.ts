import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { JsonrpcBaseService } from './jsonrpc-base.service';
import { JsonrpcResponse } from '../interfaces/jsonrpcResponse';

@Injectable({
  providedIn: 'root'
})
export class RecordingService extends JsonrpcBaseService {

  getRecordings(): Observable<JsonrpcResponse> {
    return this.executeJsonRpcRequest("getRecordings");
  }
}
