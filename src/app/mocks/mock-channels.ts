import { RawChannel } from '../interfaces/rawChannel';

export const CHANNELS: RawChannel[] = [
    {number: 1, name: 'ARD', id: '', group: ''},
    {number: 2, name: 'ZDF', id: '', group: ''},
    {number: 5, name: 'ProSieben', id: '', group: ''}
];