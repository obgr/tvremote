import { Epg } from '../models/epg';

export const TIMEEPGS: Epg[] = [
    {
        channel: "Das Erste", 
        id: 2342, 
        title: "In aller Freundschaft (907)",
        subtitle: "Club 27",
        startTime: "20:15", 
        endTime: "20:16", 
        description: "Bastian Marquardt bewirbt sich als neuer Gitarrist bei der Band der extrem populären Sängerin Luzie Herbst. Doch während seines Vorspiels bricht diese zusammen. Herzstillstand! Bastian reanimiert Luzie bis zum Eintreffen des Rettungswagens. In der Sachsenklinik stellt Maria Weber eine Verengung der Aortenklappe fest. Luzie muss dringend operiert werden, doch sie verweigert den Eingriff, da sie vor einem halben Jahr mitten in einer Schulter-Operation aus der Narkose erwachte. Sie ist traumatisiert und will lieber jetzt sterben, mit 27.",
        vpsStartTime: "20:14"
    },
    {
        channel: "ProSieben", 
        id: 2342,
        title: "The Masked Singer",
        subtitle: "The Masked Singer Musik-Show, D 2020",
        startTime: "20:15", 
        endTime: "20:16",
        description: "Moderation: Matthias Opdenhövel|In der ersten Liveshow unterstützt TV-Legende Dieter Hallervorden, der die Zuschauer im Frühjahr als leichtfüßiges Chamäleon verzückte, als Rategast \"Hase\" Sonja Zietlow und \"Heavy-Metal-Engel\" Bülent Ceylan im Rateteam. Zusammen mit allen Fans wollen die drei #MaskedSinger-Experten den zehn neuen Undercover-Sängern in ihren aufwändigen Kostümen auf die Schliche kommen.|Rateteam: Sonja Zietlow, Bülent Ceylan, Dieter Hallervorden",
        vpsStartTime: "20:14"
    }
];
