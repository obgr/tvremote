import { Epg } from '../models/epg';

export const CHANNELEPGS: Epg[] = [
    {
        channel: "Das Erste", 
        id: 2634,
        title: "In aller Freundschaft (907)",
        subtitle: "Club 27",
        startTime: "20:15", 
        endTime: "20:17", 
        description: "Bastian Marquardt bewirbt sich als neuer Gitarrist bei der Band der extrem populären Sängerin Luzie Herbst. Doch während seines Vorspiels bricht diese zusammen. Herzstillstand! Bastian reanimiert Luzie bis zum Eintreffen des Rettungswagens. In der Sachsenklinik stellt Maria Weber eine Verengung der Aortenklappe fest. Luzie muss dringend operiert werden, doch sie verweigert den Eingriff, da sie vor einem halben Jahr mitten in einer Schulter-Operation aus der Narkose erwachte. Sie ist traumatisiert und will lieber jetzt sterben, mit 27.",
        vpsStartTime: "20:14"
    }
];
