import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule, MatLabel } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { MatTreeModule } from '@angular/material/tree';
import { MatDialogModule } from '@angular/material/dialog';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';

// Own components
import { AppComponent } from './components/app.component';
import { SettingsComponent } from './components/settings/settings.component';
import { ChannelsComponent } from './components/channels/channels.component';
import { ChannelRowComponent } from './components/channel-row/channel-row.component';
import { TvprogramComponent } from './components/tvprogram/tvprogram.component';
import { TvprogramRowComponent } from './components/tvprogram-row/tvprogram-row.component';
import { TimersComponent } from './components/timers/timers.component';
import { TimerRowComponent } from './components/timer-row/timer-row.component';
import { RecordingsComponent } from './components/recordings/recordings.component';
import { RecordingRowComponent } from './components/recording-row/recording-row.component';
import { RemoteComponent } from './components/remote/remote.component';
import { EpgDescriptionDialogComponent } from './components/epg-description-dialog/epg-description-dialog.component';


@NgModule({
  declarations: [
    AppComponent,
    SettingsComponent,
    ChannelsComponent,
    ChannelRowComponent,
    TvprogramComponent,
    TvprogramRowComponent,
    TimersComponent,
    TimerRowComponent,
    RecordingsComponent,
    RecordingRowComponent,
    RemoteComponent,
    EpgDescriptionDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatIconModule,
    MatMenuModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatTreeModule,
    MatDialogModule,
    FlexLayoutModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
