import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChannelsComponent } from './components/channels/channels.component';
import { RecordingsComponent } from './components/recordings/recordings.component';
import { RemoteComponent } from './components/remote/remote.component';
import { SettingsComponent } from './components/settings/settings.component';
import { TimersComponent } from './components/timers/timers.component';
import { TvprogramComponent } from './components/tvprogram/tvprogram.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'tvprogram' }, // get this from settings
  { path: 'channels', component: ChannelsComponent },
  { path: 'settings', component: SettingsComponent },
  { path: 'tvprogram', component: TvprogramComponent },
  { path: 'timers', component: TimersComponent },
  { path: 'recordings', component: RecordingsComponent },
  { path: 'remote', component: RemoteComponent }
];

@NgModule({   
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
