import { Recording } from '../models/recording';

export interface TreeNode {
    name: string;
    children?: (TreeNode)[];
    expandable?: boolean;
    level?: number;
    number?: number;
    startDate?: string;
    startTime?: string;
    isNew?: boolean;
}
