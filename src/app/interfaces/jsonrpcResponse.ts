import { RawChannel } from './rawChannel';
import { RawEpg } from './rawEpg';
import { RawRecording } from './rawRecording';
import { RawTimer } from './rawTimer';

export interface JsonrpcResponse {
    jsonrpc: string,
    id: number,
    result?: {channels?: RawChannel[], timers?: RawTimer[], epgData?: RawEpg[], recordings?: RawRecording[]},
    error?: any
}
