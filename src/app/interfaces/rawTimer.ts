export interface RawTimer {
    number: number,
    isActive: boolean,
    isVps: boolean,
    channel: number,
    date: string,
    start: string,
    end: string,
    priority: number,
    lifeTime: number,
    name: string,
    description: string
}