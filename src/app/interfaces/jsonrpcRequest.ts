export interface JsonrpcRequest {
    jsonrpc: string,
    id: number,
    method: string,
    auth: string,
    params?: any
}
