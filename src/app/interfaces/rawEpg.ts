export interface RawEpg {
    "channel": {id: string, name: string},
    "id": number,
    "start": number,
    "duration": number,
    "title": string,
    "subtitle"?: string,
    "shortText": string,
    "description": string,
    "vpsStart": number
}