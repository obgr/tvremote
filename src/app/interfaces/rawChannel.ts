export interface RawChannel {
    number: number,
    name: string,
    id: string,
    group: string
}
