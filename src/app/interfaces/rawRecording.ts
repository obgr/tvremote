export interface RawRecording {
    number: number,
    name: string,
    startDate: string,
    startTime: string,
    isNew:boolean
}
