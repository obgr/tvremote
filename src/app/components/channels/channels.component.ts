import { Component, OnInit } from '@angular/core';
import { ChannelService } from '../../services/channel.service';
import { EpgService } from '../../services/epg.service';
import { Channel } from 'src/app/models/channel';
import { Epg } from 'src/app/models/epg';

@Component({
  selector: 'app-channels',
  templateUrl: './channels.component.html',
  styleUrls: ['./channels.component.scss']
})
export class ChannelsComponent implements OnInit {
  channels: Channel[];
  epgs: Epg[];
  selected = 1;

  constructor(private channelService: ChannelService, private epgService: EpgService) {
    // Do something
  }

  ngOnInit(): void {
    this.getChannels();
  }

  getChannels(): void {
    this.channelService.getChannels().subscribe(channels => {
      if (channels.result !== undefined) {
        this.channels = channels.result.channels.map((c) => {
          return new Channel(c);
        });
        this.getEpgs(this.selected.toString());
      } else {
        this.channels = [];
        this.epgs = [];
      }
    });
  }

  getEpgs(channel?: string): void {
    this.epgService.getEpgs(Number(channel)).subscribe(epgs => {
      if (epgs.result !== undefined) {
        this.epgs = epgs.result.epgData.map((e) => {
          return new Epg(e);
        });
      } else {
        this.epgs = [];
      }
    });
  }

  onChannelChange(): void {
    this.getEpgs(this.selected.toString());
  }
}
