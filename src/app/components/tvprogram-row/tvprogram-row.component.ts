import { Component, OnInit, Input } from '@angular/core';
import { Epg } from '../../models/epg';
import { MatDialog } from '@angular/material/dialog';
import { EpgDescriptionDialogComponent } from '../epg-description-dialog/epg-description-dialog.component';

@Component({
  selector: 'app-tvprogram-row',
  templateUrl: './tvprogram-row.component.html',
  styleUrls: ['./tvprogram-row.component.scss']
})
export class TvprogramRowComponent implements OnInit {
  @Input('epg') data: Epg;

  constructor(public dialog: MatDialog) { 
    // Do nothing
  }

  ngOnInit(): void {
    // Do nothing
  }

  openDescriptionDialog(): void {
    this.dialog.open(EpgDescriptionDialogComponent, {
      data: {
        description: this.data ? this.data.description : ""
      }
    });
  }

}