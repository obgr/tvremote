import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TvprogramRowComponent } from './tvprogram-row.component';

describe('TvprogramRowComponent', () => {
  let component: TvprogramRowComponent;
  let fixture: ComponentFixture<TvprogramRowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TvprogramRowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TvprogramRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
