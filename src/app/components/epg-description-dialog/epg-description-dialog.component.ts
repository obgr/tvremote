import { Component, OnInit, Inject } from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-epg-description-dialog',
  templateUrl: './epg-description-dialog.component.html',
  styleUrls: ['./epg-description-dialog.component.scss']
})
export class EpgDescriptionDialogComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: {description: string}) {
    // Do nothing
  }

  ngOnInit(): void {
    // Do nothing
  }

}
