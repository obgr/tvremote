import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EpgDescriptionDialogComponent } from './epg-description-dialog.component';

describe('EpgDescriptionDialogComponent', () => {
  let component: EpgDescriptionDialogComponent;
  let fixture: ComponentFixture<EpgDescriptionDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EpgDescriptionDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EpgDescriptionDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
