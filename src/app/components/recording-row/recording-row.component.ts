import { Component, Input, OnInit } from '@angular/core';
import { Recording } from 'src/app/models/recording';

@Component({
  selector: 'app-recording-row',
  templateUrl: './recording-row.component.html',
  styleUrls: ['./recording-row.component.scss']
})
export class RecordingRowComponent implements OnInit {
  @Input('recording') data: Recording;

  constructor() { 
    // Do nothing
  }

  ngOnInit(): void {
    // Do nothing
  }

}
