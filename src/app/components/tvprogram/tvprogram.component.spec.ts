import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TvprogramComponent } from './tvprogram.component';

describe('TvprogramComponent', () => {
  let component: TvprogramComponent;
  let fixture: ComponentFixture<TvprogramComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TvprogramComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TvprogramComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
