import { Component, OnInit } from '@angular/core';
import { EpgService } from '../../services/epg.service';
import { ChannelService } from 'src/app/services/channel.service';
import { Channel } from 'src/app/models/channel';
import { Epg } from 'src/app/models/epg';

@Component({
  selector: 'app-tvprogram',
  templateUrl: './tvprogram.component.html',
  styleUrls: ['./tvprogram.component.scss']
})
export class TvprogramComponent implements OnInit {
  epgs: Epg[] = [];
  channels: any = {};
  times: string[] = [
    "15:00",
    "16:00",
    "17:00",
    "18:00",
    "19:00",
    "19:30",
    "20:20",
    "21:30"
  ];
  selected = "20:20";

  constructor(private epgService: EpgService, private channelService: ChannelService) {
    // Do something
  }

  ngOnInit(): void {
    this.getChannels();
  }

  getChannels(): void {
    this.channelService.getChannels().subscribe(channels => {
      if (channels.result !== undefined) {
        channels.result.channels.forEach((c) => {
          this.channels[c.id] = new Channel(c);
        });

        this.getEpgs();
      } else {
        this.channels = undefined;
        this.epgs = [];
      }
    });
  }

  getEpgs(time?: string): void {
    if (time === undefined) {
      time = "now";
    }
    this.epgService.getEpgs(undefined, time).subscribe(epgs => {
      if (epgs.result !== undefined) {
        epgs.result.epgData.forEach((e) => {
          const epg: Epg = new Epg(e);
          if (this.channels && this.channels[epg.channel.id]) {
            epg.channel = this.channels[epg.channel.id];
            this.epgs.push(epg);
          }
        });
        this.epgs.sort((a, b) => {
          return a.channel.number - b.channel.number;
        });
      } else {
        this.epgs = [];
      }
    });
  }

  onTimeChange(): void {    
    this.getEpgs(this.selected);
  }
}
