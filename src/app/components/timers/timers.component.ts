import { Component, OnInit } from '@angular/core';
import { Timer } from 'src/app/models/timer';
import { TimerService } from 'src/app/services/timer.service';

@Component({
  selector: 'app-timers',
  templateUrl: './timers.component.html',
  styleUrls: ['./timers.component.scss']
})
export class TimersComponent implements OnInit {
  timers: Timer[] = [];

  constructor(private timerService: TimerService) { 
    // Do nothing
  }

  ngOnInit(): void {
    this.getTimers();
  }

  getTimers(): void {
    this.timerService.getTimers().subscribe(timers => {
      if (timers.result !== undefined && timers.result.timers !== undefined) {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const timersByDate: any = {};
        timers.result.timers.forEach((t) => {
          const timer = new Timer(t);
          if (timersByDate[timer.date] === undefined) {
            timersByDate[timer.date] = [];
          }
          timersByDate[timer.date].push(timer);
        });
        const timerDates: string[] = Object.keys(timersByDate);
        timerDates.sort();
        for (let i=0; i<timerDates.length; i++) {
          const label = timerDates[i];
          // eslint-disable-next-line no-prototype-builtins
          if (timersByDate.hasOwnProperty(label)) {
            timersByDate[label].sort((a, b) => {
              return new Date(`${a.date} ${a.startTime}`).getTime() - new Date(`${b.date} ${b.startTime}`).getTime();
            });
            const firstEntry = timersByDate[label][0];
            const days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
            const startDate = new Date(`${firstEntry.date} ${firstEntry.startTime}`);
            firstEntry.startDay = `${days[startDate.getDay()]}, ${startDate.toLocaleDateString()}`;
            timersByDate[label].forEach((dt) => {
              this.timers.push(dt);
            });
          }
        }
      }
    });
  }

}
