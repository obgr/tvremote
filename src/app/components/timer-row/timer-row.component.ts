import { Component, OnInit, Input } from '@angular/core';
import { Timer } from 'src/app/models/timer';

@Component({
  selector: 'app-timer-row',
  templateUrl: './timer-row.component.html',
  styleUrls: ['./timer-row.component.scss']
})
export class TimerRowComponent implements OnInit {
  @Input('timer') data: Timer;

  constructor() { 
    // Do nothing
  }

  ngOnInit(): void {
    // Do nothing
  }

}
