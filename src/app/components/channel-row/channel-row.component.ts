import { Component, OnInit, Input } from '@angular/core';
import { Epg } from '../../models/epg';

@Component({
  selector: 'app-channel-row',
  templateUrl: './channel-row.component.html',
  styleUrls: ['./channel-row.component.scss']
})
export class ChannelRowComponent implements OnInit {
  @Input('epg') data: Epg;
  @Input('isFirst') isFirst: boolean;

  constructor() { 
    // Do nothing
  }

  ngOnInit(): void {
    // Do nothing
  }

}
