import { FlatTreeControl } from '@angular/cdk/tree';
import { Component, OnInit } from '@angular/core';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { TreeNode } from 'src/app/interfaces/treeNode';
import { Recording } from 'src/app/models/recording';
import { RecordingService } from 'src/app/services/recording.service';

@Component({
  selector: 'app-recordings',
  templateUrl: './recordings.component.html',
  styleUrls: ['./recordings.component.scss']
})
export class RecordingsComponent implements OnInit {

  private _transformer = (node: TreeNode, level: number) => {
    node.expandable = !!node.children && node.children.length > 0;
    node.level = level;
    return node;
  }

  treeControl = new FlatTreeControl<TreeNode>(node => node.level, node => node.expandable);

  treeFlattener = new MatTreeFlattener(this._transformer, node => node.level, node => node.expandable, node => node.children);

  dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

  constructor(private recordingService: RecordingService) {
    //
   }

  ngOnInit(): void {
    this.getRecordings();
  }

  getRecordings(): void {
    this.recordingService.getRecordings().subscribe(rawRecordings => {

      const folderList: TreeNode[] = [{
        name: 'Unsorted',
        children: []
      }];
      if (rawRecordings.result !== undefined) {
        const recordings = rawRecordings.result.recordings.map((r) => {
          return new Recording(r);
        });
        recordings.sort((a, b) => {
          if (a.name > b.name) {
            return 1;
          } else if (a.name < b.name) {
            return -1;
          }
          return 0;
        });
        recordings.forEach((r) => {
          const nameParts = r.name.split('~');
          if (nameParts.length === 1) {
            this.addRecordingToUnsortedFolder(r, folderList);
          }
          this.addSubFolder(r, folderList);
        });
      }
      this.sortRecordings(folderList);
      this.dataSource.data = folderList;
    });
  }

  isFolder(_mNumber: number, node: TreeNode): boolean {
    return node.startDate === undefined;
  }

  private addRecordingToUnsortedFolder(recording: Recording, folderList: TreeNode[]) {
    folderList.forEach((f) => {
      if (f.name === 'Unsorted') {
        f.children.push(recording);
      }
    });
  }

  private addSubFolder(recording: Recording, folderList: TreeNode[]) {
    const nameParts = recording.name.split('~');    
    if (nameParts.length > 1) {
      const folderName = nameParts.shift();
      recording.name = nameParts.join('~');
      let folderExists = false;
      folderList.forEach((f) => {
        if ("children" in f) {
          if (f.name === folderName) {
            this.addSubFolder(recording, f.children);
            folderExists = true;
          }
        }
      });
      if (!folderExists) {
        const newFolder: TreeNode = {
          name: folderName,
          children: []
        };
        folderList.push(newFolder);
        this.addSubFolder(recording, newFolder.children);
      }
    } else {
      let itemExists = false;
      folderList.forEach((f) => {
        if ("number" in f) {
          if (f.name === recording.name && f.number === recording.number) {
            itemExists = true;
          }
        }
      });
      if (!itemExists) {
        folderList.push(recording);
      }
    }
  }

  private sortRecordings(folderList: TreeNode[]) {
    folderList.forEach((f) => {
      if (!!f.children && f.children.length > 0) {
        this.sortRecordings(f.children);
      }
    });
    folderList.sort((a, b) => {
      if (a.startDate !== undefined && b.startDate !== undefined) {
        const aStart = new Date(`${a.startDate} ${a.startTime}`);
        const bStart = new Date(`${b.startDate} ${b.startTime}`);
        return bStart.getTime() - aStart.getTime();
      }
    });
  }
}
